HELLO WORLD!

## Goals

The project has two goals

* Explore homotopical mathematics
* Experiment with efficient tools for open, collaborative mathematics

There is already a substantial literature on homotopical mathematics. Our goal is to explore a few themes, including:

* Using Homotopy type theory (HoTT) as foundations for the Fukaya-Kontsevich-Lurie style mathematics.
* Explore Sullivan's idea concerning simplicial models for geometric PDEs
* Understand the role of HoTT beyond the Calculus of Inductive constructions in formal mathematics and software

## Design

Our goal is to make it easy to get off the ground quickly (even for collaborators) while having a prfductive and powerful design going forward (for example, having all the notes browsable by a web page).

## Contributing

The quickest way to start contributing is to use the [issue tracker](https://gitlab.com/siddhartha.gadgil/homotopical-mathematics/issues) to create issues or comment on exsting ones. Details of this and more substantial ways to contribute are in the [wiki](https://gitlab.com/siddhartha.gadgil/homotopical-mathematics/wikis/home).

## Note files

Most of the contents of this repository should be mathematical notes in the \_notes folder, with meta issues in the wiki. These should ideally have _topmatter_ like a Jekyll file, including a title. For now, _markdown_ and _html_ are accepted formats and have TeX support. If there is a good enough reason to support, say, full latex, then please raise an issue.

### Latex support

Gitlab has been chosen partly because of its [latex support](https://docs.gitlab.com/ee/user/markdown.html#math), e.g. $`3+ 4^2 = \sqrt{x}`$
is written as ``$`3+ 4^2 = \sqrt{x}`$``. This includes both the main (i.e., _code_) files and the issues, wiki etc.
